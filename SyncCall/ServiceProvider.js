﻿const serviceProxy = {}

function setServiceBridge(serviceBridgeObject) {
    log('setServiceBridge invoked in script from server');
    if (serviceBridgeObject !== null)
        serviceProxy.ServiceBridge = serviceBridgeObject;
}

function IsModified() {
    log('Invoked IsModified from server');
    var rand = Math.floor((Math.random() * 10) + 1);
    var returnValue = rand % 2 == 0;
    log('IsModified = ' + returnValue);
    return returnValue;
}

function SaveData() {
    log('SaveData() JS method invoked from C# using ExecuteScriptAsync');
    log('Invoking SaveDataOnServer method in C# ServiceBridge using service proxy from JS');
    serviceProxy.ServiceBridge.SaveDataOnServer();
}

const logArea = document.querySelector("#log");

function log(message) {
    if (logArea)
        logArea.value += message + '\n';
}

var ObjectServiceProvider = function () {
};

$(function () {
    console.log('JQuery document ready called');
    const serviceProvider = new ObjectServiceProvider();
    serviceProxy.ServiceProvider = serviceProvider;
});