﻿using System.Runtime.InteropServices;

namespace SyncCall
{
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [ComVisible(true)]
    public class ServiceBridge
    {
        public ServiceBridge(HtmlHost browser)
        {
            Browser = browser;
        }

        public HtmlHost Browser { get; }

        //public async void SaveDataOnServer()
        //{
        //    var retVal = await Utility.Instance.CallServiceProviderAsync<string>("IsModified");
        //    Utility.Instance.ShowMessageInMainWindow($"IsModified = {retVal}");
        //}

        public void SaveDataOnServer()
        {
            var retVal = AsyncUtil.WaitWithMessagePumping(Utility.Instance.CallServiceProviderAsync<string>("IsModified"));
            Utility.Instance.ShowMessageInMainWindow($"IsModified = {retVal}");
        }
    }
}