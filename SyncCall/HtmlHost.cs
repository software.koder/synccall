﻿using Microsoft.Web.WebView2.Core;
using Microsoft.Web.WebView2.Wpf;

using Newtonsoft.Json;

using System;
using System.Threading.Tasks;

namespace SyncCall
{
    public class HtmlHost
    {
        private WebView2 _webView;
        private const string HOST_OBJECT_NAME_FOR_SCRIPT = "ServiceBridge";
        private ServiceBridge _serviceBridge;
        private Uri _navigationUri = new Uri(AppDomain.CurrentDomain.BaseDirectory + @"..\..\..\html\display.html");

        public WebView2 Browser { get => _webView; private set => _webView = value; }

        public HtmlHost()
        {
            InitializeWebView();
        }

        private async Task InitializeWebView()
        {
            var v = GetVersion();

            _webView = new WebView2();

            _webView.Width = 1200;
            _webView.Height = 840;

            _webView.CoreWebView2InitializationCompleted += (s, e) =>
            {
                if (e.IsSuccess)
                {
                    RegisterHostObject();
                    RegisterBrowserEventHandlers();
                    NavigateToUrl(_navigationUri.ToString());
                }
            };
            await _webView.EnsureCoreWebView2Async();
        }

        private Version GetVersion()
        {
            string runtimeString = CoreWebView2Environment.GetAvailableBrowserVersionString();

            // Microsoft Edge Insider Channels can be used instead of the Evergreen runtime.
            // These versions will be appended with "beta", "dev", or "canary". Strip the channel name from the version string.
            int delimiterPosition = runtimeString.IndexOf(" ");
            string baseRuntimeString = delimiterPosition >= 0 ? runtimeString.Substring(0, delimiterPosition) : runtimeString;

            Version runtimeVersion = new Version(baseRuntimeString);

            return runtimeVersion;
        }

        private void RegisterHostObject()
        {
            _serviceBridge = new ServiceBridge(this);
            _webView.CoreWebView2.AddHostObjectToScript(HOST_OBJECT_NAME_FOR_SCRIPT, _serviceBridge);
        }

        private void RegisterBrowserEventHandlers()
        {
            _webView.CoreWebView2.DOMContentLoaded += (s, e) =>
            {
                SetServiceProviderInScript();
            };
        }

        private async void SetServiceProviderInScript()
        {
            //_webView.CoreWebView2.OpenDevToolsWindow();
            var serviceBridgeHostObjectProxyName = GetHostObjectProxyName(HOST_OBJECT_NAME_FOR_SCRIPT);
            await RunScriptAsync($"setServiceBridge({serviceBridgeHostObjectProxyName});").ConfigureAwait(false);
        }

        private void NavigateToUrl(string url)
        {
            _webView.CoreWebView2.Navigate(url);
        }

        private string GetHostObjectProxyName(string hostObjectName)
        {
            return $"window.chrome.webview.hostObjects.{hostObjectName}";
        }

        private string GetSyncHostObjectProxyName(string hostObjectName)
        {
            return $"window.chrome.webview.hostObjects.sync.{hostObjectName}";
        }

        public async Task<object> ExecuteMethodAsync(string method, params object[] methodParams)
        {
            if (_webView?.CoreWebView2 == null)
            {
                return null;
            }

            string script = method + "(";
            for (int i = 0; i < methodParams.Length; i++)
            {
                script += JsonConvert.SerializeObject(methodParams[i]);
                if (i < methodParams.Length - 1)
                {
                    script += ", ";
                }
            }
            script += ");";

            return await _webView.ExecuteScriptAsync(script).ConfigureAwait(false);
        }

        public async Task<object> RunScriptAsync(string script, TimeSpan? timeout = null)
        {
            if (_webView?.CoreWebView2 == null)
            {
                return null;
            }
            return await _webView.ExecuteScriptAsync(script);
        }
    }
}