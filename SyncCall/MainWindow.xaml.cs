﻿using System.Windows;

namespace SyncCall
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private HtmlHost _htmlHost;

        public MainWindow()
        {
            InitializeComponent();

            _htmlHost = new HtmlHost();
            Content.Children.Add(_htmlHost.Browser);

            this.DataContext = new MainViewModel(_htmlHost);

            Utility.Instance.SetBrowserInstance(_htmlHost.Browser);
            Utility.Instance.SetWindowInstance(this);
        }

        public void DisplayMessage(string msg)
        {
            MessageBox.Show(msg);
        }
    }
}