﻿using Microsoft.Web.WebView2.Wpf;

using Newtonsoft.Json;

using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace SyncCall
{
    public sealed class Utility
    {
        private WebView2 _browser;
        private MainWindow _window;

        private Utility()
        {
        }

        private static readonly Lazy<Utility> lazy =
        new Lazy<Utility>(() => new Utility());

        public static Utility Instance
        { get { return lazy.Value; } }

        public void SetBrowserInstance(WebView2 browser)
        {
            _browser = browser;
        }

        public void SetWindowInstance(MainWindow window)
        {
            _window = window;
        }

        public async Task<string> ExecuteMethodAsync(string method, params object[] methodParams)
        {
            string script = method + "(";
            for (int i = 0; i < methodParams.Length; i++)
            {
                script += JsonConvert.SerializeObject(methodParams[i]);
                if (i < methodParams.Length - 1)
                {
                    script += ", ";
                }
            }
            script += ");";

            return await _browser.ExecuteScriptAsync(script).ConfigureAwait(false);
        }

        public async Task<T> CallServiceProviderAsync<T>(string scriptName, params object[] args)
        {
            T result = default(T);
            try
            {
                object invokeResult = await InvokeScriptAsync<T>(scriptName, args: args).ConfigureAwait(false);

                result = (T)invokeResult;
            }
            catch (Exception ex)
            {
            }

            return result;
        }

        public Task<T> InvokeScriptAsync<T>(string scriptName, params object[] args)
        {
            return InvokeMethodAsync<T>(scriptName, args);
        }

        public async Task<T> InvokeMethodAsync<T>(string scriptName, params object[] args)
        {
            string script = scriptName + "(";
            for (int i = 0; i < args.Length; i++)
            {
                script += JsonConvert.SerializeObject(args[i]);
                if (i < args.Length - 1)
                {
                    script += ", ";
                }
            }
            script += ");";

            var result = await _browser.ExecuteScriptAsync(script).ConfigureAwait(false);

            return (T)(TypeDescriptor.GetConverter(typeof(T)))?.ConvertFromString(result);
        }

        public void ShowMessageInMainWindow(string msg)
        {
            _window.DisplayMessage(msg);
        }
    }
}