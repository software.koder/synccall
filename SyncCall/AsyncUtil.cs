﻿using System.Threading.Tasks;
using System.Windows.Threading;

namespace SyncCall
{
    public static class AsyncUtil
    {
        public static T WaitWithMessagePumping<T>(Task<T> asyncAction)
        {
            DispatcherFrame frame = new DispatcherFrame();
            asyncAction.ContinueWith(t => frame.Continue = false);
            Dispatcher.PushFrame(frame);
            return asyncAction.GetAwaiter().GetResult();
        }
    }
}