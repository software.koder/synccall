﻿using System.ComponentModel;
using System.Windows.Input;

namespace SyncCall
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private ICommand _saveDataCommand;
        private readonly HtmlHost _htmlHost;

        public MainViewModel(HtmlHost htmlHost)
        {
            _htmlHost = htmlHost;
        }

        public ICommand SaveDataCommand
        {
            get
            {
                return _saveDataCommand ??= new RelayCommand(
                        param => this.SaveData(),
                        param => this.CanSaveData()
                    );
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private bool CanSave()
        {
            return true;
        }

        private bool CanSaveData()
        {
            return true;
        }

        private void SaveData()
        {
            _htmlHost.Browser.ExecuteScriptAsync("SaveData();").ConfigureAwait(false);
        }
    }
}